﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;


public class WorldCursor : MonoBehaviour
{
    private MeshRenderer cursormeshRenderer;
    private MeshRenderer meshRenderSelected;
    private MeshRenderer lastSelected;

    StreamWriter file2;

    public GameObject gobjinicial;

    string dttime, pathfile;
    

    




    // Use this for initialization

    private void Awake()
    {
        CreateFiletoToLog();
        


    }

    private void CreateFiletoToLog()
    {
        dttime = System.DateTime.Now.ToString();

        string a = Regex.Replace(dttime, "[@&'(\\s)<>#:/]", "");

        pathfile = @"Registros\reg" + a + ".txt";

        //Debug.Log("Nome do arguivo: " + pathfile);

        File.Create(pathfile);

    }

    void Start()
    {
        // Grab the mesh renderer that's on the same object as this script.
        cursormeshRenderer = this.gameObject.GetComponentInChildren<MeshRenderer>();

        meshRenderSelected = gobjinicial.GetComponentInChildren<MeshRenderer>();
        lastSelected = meshRenderSelected;



    }

    // Update is called once per frame
    void Update()
    {
        // Do a raycast into the world based on the user's
        // head position and orientation.
        var headPosition = Camera.main.transform.position;
        var gazeDirection = Camera.main.transform.forward;

        
        

        RaycastHit hitInfo;

        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo))
        {
            //Debug.Log( "Atingiu - " + meshRenderSelected.tag);

            //descobre em qual objeto deu hit e habilita 
            meshRenderSelected = hitInfo.collider.gameObject.GetComponentInChildren<MeshRenderer>();

            lastSelected = meshRenderSelected;

            //string nomeobjeto = transform.parent.name;

                cursormeshRenderer.enabled = true;

            if (lastSelected.tag == "Audi" 
                || lastSelected.tag == "Viper" 
                || lastSelected.tag == "Lamborguini" 
                || lastSelected.tag == "Porsche" 
                || lastSelected.tag == "Ferrari" 
                || lastSelected.tag == "Ferrari458"
                || lastSelected.tag == "BannerAudi"
                || lastSelected.tag == "BannerPorsche"
                || lastSelected.tag == "BannerLamborguini"
                || lastSelected.tag == "VideoAudi"
                || lastSelected.tag == "VideoFerrari458"
                || lastSelected.tag == "Batmobil"
                || lastSelected.tag == "IronMan"
                )
            {
            // If the raycast hit a hologram...
            // Display the cursor mesh.

                //render painel de colisão.
                meshRenderSelected.enabled = true;


                //Debug.Log(System.DateTime.Now);



                //write on file txt 
                //string infos = System.DateTime.Now + ";" + Time.realtimeSinceStartup + ";" + headPosition.x + ";" + headPosition.z + ";" + meshRenderSelected.tag + "\n";
                string infos = System.DateTime.Now + ";" + Time.realtimeSinceStartup + ";" + headPosition.x + ";" + headPosition.z + ";" + meshRenderSelected.tag +";" + hitInfo.collider.gameObject.transform.position.x + ";" + hitInfo.collider.gameObject.transform.position.z + "\n";


                file2 = new StreamWriter(pathfile, true);
                file2.WriteLine(infos);
                file2.Close();

                // File.AppendAllText(arquivo, infos);


            



            }


            // Move the cursor to the point where the raycast hit.
            this.transform.position = hitInfo.point;

            // Rotate the cursor to hug the surface of the hologram.
            this.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
        }
        else
        {
            // If the raycast did not hit a hologram, hide the cursor mesh.
            cursormeshRenderer.enabled = false;


            //esconde mesh ao perder foco se for painel
            //if (lastSelected.tag == "Audi" || lastSelected.tag == "Viper" || lastSelected.tag == "Lamborguini" || lastSelected.tag == "Porsche" || lastSelected.tag == "Ferrari" || lastSelected.tag == "Ferrari458")
            if (lastSelected.tag == "Audi"
                || lastSelected.tag == "Viper"
                || lastSelected.tag == "Lamborguini"
                || lastSelected.tag == "Porsche"
                || lastSelected.tag == "Ferrari"
                || lastSelected.tag == "Ferrari458"
                || lastSelected.tag == "Batmobil"
                || lastSelected.tag == "IronMan"
                )
            {
                lastSelected.enabled = false;
                //Debug.Log("Desativou o Painel");
            }
           

        }
    }

    
}