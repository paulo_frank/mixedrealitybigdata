﻿using UnityEngine;
using UnityEngine.Video;

using HoloToolkit.Unity.InputModule;
using System;

public class BotaoClicavel : MonoBehaviour, IInputClickHandler, IInputHandler, IFocusable
{

    public Material playButtonMaterial;
    public Material pauseButtonMaterial;


    Renderer mesh;
    VideoPlayer videoPlayer;

    int clicado = -1;

    private void Start()
    {

        mesh = GetComponent<Renderer>();
        videoPlayer = GetComponentInParent<VideoPlayer>();




    }
    public void OnFocusEnter()
    {
        //mesh.material.color = Color.green;
        

    }

    public void OnFocusExit()
    {

        //mesh.material.color = Color.white;
    }

    public void OnInputClicked(InputClickedEventData eventData)
    {
        Debug.Log("-----------------------------------------------------------Clicado");
        clicado *= -1;



    }

    public void OnInputDown(InputEventData eventData)
    {

    }

    public void OnInputUp(InputEventData eventData)
    {

    }

    private void Update()
    {
        CheckVideoPlayer();

        if (clicado == 1)
        {
            //transform.Rotate(new Vector3(5, 5, 5) * Time.deltaTime);

        }
    }

    private void CheckVideoPlayer()
    {
        if (clicado == 1)
        {
            videoPlayer.Play();
            mesh.material = pauseButtonMaterial;

        }
        else
        {
            videoPlayer.Pause();
            mesh.material = playButtonMaterial;
        }
    }

    public void OnInpuClicked(InputEventData eventData)
    {
        
    }
}
